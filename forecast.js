//http://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&APPID=9aa905f9841d8d5b68f345f737d1c428

const key = "0f249a795aea31b148e051d23e99be86";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&APPID=${key}`;
    
    const response = await fetch(base+query);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error("Error Status:"+ response.status);
    }
}
getForecast()
    .then(data => console.log(data))
    .catch(err => console.log(err));